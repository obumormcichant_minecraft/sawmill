# Sawmill

This Minecraft data pack adds recipes to the stonecutter for most wood
items.  This allows a more efficient conversion from planks or logs
than going through the crafting table which may take multiple steps or
odd numbers of ingredients.

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61).

## Recipes

* Buttons
  * 1 plank → 8 buttons
  * 1 log → 32 buttons
* Fences
  * 1 plank → 1 fence
  * 1 log → 4 fences
* Gates
  * 1 log → 1 fence gate
* Pressure Plates
  * 1 plank → 4 pressure plates
  * 1 log → 16 pressure plates
* Signs
  * 1 plank -> 1 sign
  * 1 log → 4 signs
  * 1 plank → 1 hanging signs
  * 1 log → 4 hanging signs
* Slabs
  * 1 plank → 2 slabs
  * 1 log → 8 slabs
* Stairs
  * 1 plank → 1 stair
  * 1 log → 4 stairs
* Trap doors
  * 1 plank → 3 trap doors
  * 1 log → 12 trap doors

All recipes above are available for all Minecraft 1.20 and new 1.21.4
wood types: acacia, bamboo, birch, cherry, crimson, dark oak, jungle,
mangrove, oak, pale oak (1.21.2+), spruce, and warped.  In addition,
there is a recipe to cut a bamboo block back down to the 9 bamboo
stalks used to craft it.

## Usage

### New worlds

From the Create New World screen, go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy `Sawmill-`_version_`.zip` into
this folder, then it will appear in the list of “Available” data
packs.  Click the rightward arrow on the data pack icon to select it
for your world.

### Existing worlds

From the “Select World” screen, click on your world (don’t click its
play button) then click the “Edit” button.  Click the “Open World
Folder” and then open the `datapacks` folder within it.  Copy
`Sawmill-`_version_`.zip` into this folder.  Return to Minecraft’s
Edit World screen and click “Cancel”.  Then start your world; it will
automatically load the new data pack.
